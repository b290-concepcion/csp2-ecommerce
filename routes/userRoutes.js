const express=require("express");
const router = express.Router();
const auth=require("../auth");
const userController=require("../controllers/userController.js");
const productController=require("../controllers/productController.js")
const Product = require("../models/Product");
const User = require("../models/User");


router.put("/updateUser",(req,res)=>{
	const isLoggedIn = req.headers.authorization;

	if(isLoggedIn!=undefined&&isLoggedIn!=null){
let data={userId: auth.decode(req.headers.authorization).id}


	userController.updateUser(data, req.body).then(resultFromController=>res.send(resultFromController))
	} else {
		console.log({ auth : "User not logged in"})
		res.send(false);
	}
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});
//Router for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//user login
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))

})
//update user details
router.put("/updateUser",(req,res)=>{
	const isLoggedIn = req.headers.authorization;

	if(isLoggedIn!=undefined&&isLoggedIn!=null){
let data={userId: auth.decode(req.headers.authorization).id}


	userController.updateUser(data, req.body).then(resultFromController=>res.send(resultFromController))
	} else {
		console.log({ auth : "User not logged in"})
		res.send(false);
	}
})


//user can update their password
router.put("/updatePassword",(req,res)=>{
	const isLoggedIn = req.headers.authorization;

	if(isLoggedIn!=undefined&&isLoggedIn!=null){
let data={userId: auth.decode(req.headers.authorization).id}


	userController.updatePassword(data, req.body).then(resultFromController=>res.send(resultFromController))
	} else {
		console.log({ auth : "User not logged in"})
		res.send(false);
	}
})



router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({ userId : userData.id }).then(resultFromController => res.send(resultFromController));

});

router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getMyOrders({ userId : userData.id }).then(resultFromController => res.send(resultFromController));

});


//order products
router.post("/checkout", auth.verify, async (req, res) => {
  const isLoggedIn = req.headers.authorization;
  const userData = auth.decode(req.headers.authorization);

  if (isLoggedIn !== undefined && isLoggedIn !== null) {
    if (!userData.isAdmin) {
      const orders = Array.isArray(req.body) ? req.body : [req.body];
      const results = [];

      for (const item of orders) {
        const data = {
          userId: auth.decode(req.headers.authorization).id,
          userEmail: auth.decode(req.headers.authorization).email,
          productId: item.productId,
          productName: "",
          quantity: item.quantity,
          price: "",
        };

        try {
          const product = await Product.findById(item.productId);
          data.productName = product.name;
          data.price = product.price;

          const result = await userController.checkout(data);
          results.push(result);
        } catch (error) {
          console.log(error);
          // Handle the error as per your requirements
          results.push({ error: "An error occurred" });
        }
      }

      res.send(results);
    } else {
      console.log({ auth: "unauthorized user" });
      res.send(false);
    }
  }
});

//get all orders
router.get("/orders",auth.verify,(req,res)=>{ 
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	userController.getAllOrders().then(resultFromController=>res.send(resultFromController))
} else {
		console.log({ auth : "unauthorized user"})
		res.send(false);
	}	


})

//get user details
router.post("/user",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	userController.getUser(req.body.id).then(resultFromController=>res.send(resultFromController))
	} else {
		console.log({ auth : "User is unauthorized"})
		res.send(false);
	}
})


// set as admin
router.put("/user/setAdmin",(req,res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	userController.setAdmin(req.body.id).then(resultFromController=>res.send(resultFromController))
	} else {
		console.log({ auth : "User is unauthorized"})
		res.send(false);
	}
})



module.exports=router;

















