const express = require("express");
const auth=require("../auth");
const router = express.Router();

const productController = require("../controllers/productController");
const userController = require("../controllers/userController");


//route to get all active products
router.get("/",(req,res)=>{ 

	productController.getAllActiveProducts().then(resultFromController=>res.send(resultFromController))


})

// Route for creating a product
router.post("/", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.addProduct(userData, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		console.log({ auth : "unauthorized user"})
		res.send(false);
	}	


});

//route to get all active products
router.get("/all",auth.verify,(req,res)=>{ 
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController))
} else {
		console.log({ auth : "unauthorized user"})
		res.send(false);
	}	


})

router.get("/userOrders",auth.verify,(req,res)=>{ 
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	productController.getOrders().then(resultFromController=>res.send(resultFromController))
} else {
		console.log({ auth : "unauthorized user"})
		res.send(false);
	}	

})



// Route for retrieving a specific product
router.get("/:productId",(req,res)=>{

	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController))
})

//Route to update product
router.put("/:productId",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController))

	} else {
		console.log({ auth : "unauthorized user"})
		res.send(false);
	}	
})

//Route for archiving products
router.patch("/:productId/archive", auth.verify,(req,res)=>{
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin){
	productController.archiveProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController))
} else {
		console.log({ auth : "unauthorized user"})
		res.send("unauthorized user");
	}
})

//Route for activating products
router.patch("/:productId/activate", auth.verify,(req,res)=>{
		const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(isAdmin){
	productController.activateProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController))
} else {
		console.log({ auth : "unauthorized user"})
		res.send("unauthorized user");
	}
})


module.exports = router;






