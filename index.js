const express=require("express");
const mongoose=require("mongoose");

const cors=require("cors");

const userRoute=require("./routes/userRoutes.js");
const productRoute=require("./routes/productRoute");
const app=express();

//connection mongo db
mongoose.connect("mongodb+srv://admin:admin123@zuitt.e8xgdwe.mongodb.net/ecommerce?retryWrites=true&w=majority",
{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

mongoose.connection.once("open",()=>console.log("Now connected to MongoDB Atlas."))

//Middlewares

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Routes

app.use("/users",userRoute);

app.use("/products",productRoute);

if(require.main===module){
	app.listen(process.env.PORT||4000,()=>{
		console.log(`API is now online on port ${process.env.PORT||4000}`)
	})
}

module.exports={app,mongoose};












