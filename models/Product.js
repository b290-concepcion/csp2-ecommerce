const mongoose=require("mongoose");

const productSchema=new mongoose.Schema({

	name:{
		type:String,
		required:[true, "Name is required"]
	},
	description:{
		type:String,
		required:[true, "description is required"]
	},
	price:{
		type:Number,
		required:[true, "price is required"]
	},	
	imageUrl:{
		type:String,
	},
	addedBy:{
		type:String,

	},
	isActive:{
		type:Boolean,
		default:true
	},

	createdOn:{
	type:Date,
	default:new Date()
	},



userOrders:[
{
		userId:{
		type:String,
		required:[true, "user Id is required"]
	},
		userEmail:{
		type:String,
		required:[true, "user Email is required"]
	},
		quantity:{
		type:Number,
		required:[true, "quantity is required"]
	}

}
	]



})

module.exports=mongoose.model("Product",productSchema)