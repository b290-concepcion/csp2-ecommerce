const mongoose=require("mongoose");

const userSchema=new mongoose.Schema({

	firstName:{
		type:String,
		required:[true, "firstName is required"]
	},
	lastName:{
		type:String,
		required:[true, "lastName is required"]
	},
	email:{
		type:String,
		required:[true, "Email is required"]
	},
	password:{
		type:String,
		required:[true, "Password is required"]
	},

	isAdmin:{
		type:Boolean,
		default:false
	},
	points:{
		type:Number,
		default:0
	},


orderedProduct:[
{
	products:[
	{
		productId:{
		type:String,
		required:[true, "Product Id is required"]
	},
		productName:{
		type:String,
		required:[true, "Product name is required"]

	},
		quantity:{
		type:Number,
		required:[true, "quantity is required"]
	}
	}
	],
	totalAmount:Number,
	purchasedOn:{
	type:Date,
	default:new Date()
	}
}
	]



})

module.exports=mongoose.model("User",userSchema)