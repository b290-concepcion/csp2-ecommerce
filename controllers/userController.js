const bcrypt = require("bcrypt");

const auth=require("../auth");

const Product = require("../models/Product");

const User = require("../models/User");


module.exports.checkEmailExists = (reqBody) => {

	return User.find({ email : reqBody.email }).then(result => {

		if(result.length > 0){

			return true;
		} else {

			return false;
		}
	}).catch(err => {

				console.log(err);

				return false;
		});
};


// User registration

module.exports.registerUser=reqBody=>{

	let newUser=new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,


		password: bcrypt.hashSync(reqBody.password,10,)
	});

	return newUser.save().then(user=>true).catch(err => {

				console.log(err);

				return false;
			});
}

//user authentication(login)


module.exports.loginUser=reqBody=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		console.log(result);
		if(result==null){
			return false; 


		} else {
			const isPasswordCorrect=bcrypt.compareSync(reqBody.password,result.password);


			if(isPasswordCorrect){
				console.log(result)
				return{access:auth.createAccessToken(result)}

			} else {
				console.log()
				return false;
			}
		}
	}).catch(err=>{console.log(err); return false})
}



module.exports.getUser =reqBody => {
  return User.findById(reqBody)
    .then((result) => {
      console.log(result);
      return result;
    })
    .catch((err) => {
      console.log(err);
      return false;
    });
};

module.exports.getMyOrders =(data) => {

	return User.findById(data.userId).then(result => {
		console.log(result)

		return result.orderedProduct;

	}).catch(err => {

		console.log(err);

		return false
	});
};

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		console.log(result)

		return result;

	}).catch(err => {

		console.log(err);

		return false
	});

};

//user can update their details
module.exports.updateUser=(data,reqBody)=>{

	let updatedUser= {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,

	};

	return User.findByIdAndUpdate(data.userId,updatedUser).then(Userupdate=>true).catch(saveErr=>{console.log(saveErr) 
	return "failed to update user"});
}

//user can update their password
module.exports.updatePassword=(data,reqBody)=>{

	let updatedPassword= {
		password: bcrypt.hashSync(reqBody.password,10,)

		
	};

	return User.findByIdAndUpdate(data.userId,updatedPassword).then(Userupdate=>true).catch(saveErr=>{console.log(saveErr) 
	return "failed to update user's password"});
}

//admin can change user's admin status
module.exports.setAdmin=(reqBody)=>{
	let updatedUser= {
		isAdmin: true,

	};

	return User.findByIdAndUpdate(reqBody,updatedUser).then(Userupdate=>true).catch(saveErr=>{console.log(saveErr) 
	return "failed to set user as admin"});
}


// order a product
module.exports.checkout=async (data)=>{


	let isUserUpdated= await User.findById(data.userId).then(user=>{
		let pointsApplied=parseInt(user.points / 1000)
		let discount= pointsApplied* 50;
		let total=(data.price*data.quantity)-discount;
		let pointsEarned= parseInt(total/20)
		let updatedPoints = user.points - (parseInt(user.points/1000)*1000)+pointsEarned;
		user.orderedProduct.push({
			totalAmount: total,
			products: [
        {
          productId: data.productId,
          productName: data.productName,
          quantity: data.quantity,
          
        }]
       
	});
		

		return user.save().then(user=> {return [true, updatedPoints];
}).catch(err=>{
			console.log(err);
			return [false, updatedPoints]
		})
	}).catch(err=>{
		console.log(err);
		console.log("error in retrieving user");

		return [false, updatedPoints]
	});

let isProductUpdated= await Product.findById(data.productId).then(product=>{

	product.userOrders.push({
		userId: data.userId,
		userEmail:data.userEmail,
		quantity: data.quantity
		        
		      	

	});

	return product.save().then(product=>true).catch(err=>{
			console.log(err);
			return false
		})
}).catch(err=>{
		console.log(err);
		console.log("error in retrieving product");

		return false
	})

let updatedUser= {
		points: isUserUpdated[1],

	};

	return User.findByIdAndUpdate(data.userId,data.userEmail,updatedUser).then(Userupdate=>true).catch(saveErr=>{console.log(saveErr) 
	return "failed to update user"});


return (isUserUpdated[0]&&isProductUpdated) ? true:false;

 

}


//get all orders
module.exports.getAllOrders = async () => {
  try {
    const users = await User.find({ orderedProduct: { $exists: true, $ne: [] } });

    if (users.length > 0) {
      const orders = users.map(user => user.orderedProduct);
      return orders;
    } else {
      console.log("No users found with ordered products");
      return [];
    }
  } catch (error) {
    console.log(error);
    console.log("Error in retrieving users");
    return false;
  }
};



