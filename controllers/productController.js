const Product = require("../models/Product");
const auth=require("../auth");
// Create a new product


module.exports.addProduct = (userData,reqBody) => {

	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		addedBy: userData.email
	});

	// Saves the created object to our database
	return newProduct.save().then(product => true)
		.catch(error => {

			console.log(error);

			return false
		})
}

//get all products
module.exports.getAllProducts=()=>{


return Product.find({}).then(result=>{
	return result;
}).catch(saveErr=>{console.log(saveErr) 
	return false});

}

//get all active products
module.exports.getAllActiveProducts = () => {
return Product.find({isActive:true})
.then((result) => {return result;
})
.catch((error) => {
console.log(error);
return false;
});
};
// Retrieving a specific product

module.exports.getProduct=reqParams=>{
	return Product.findById(reqParams.productId).then(result=>result).catch(err=>{
		console.log(err)
		return false;
	});
};


// Update a product

module.exports.updateProduct=(reqParams,reqBody)=>{
	let updatedProduct={
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then(product=>true).catch(saveErr=>{console.log(saveErr) 
	return false});
}


//archive a product
module.exports.archiveProduct=(reqParams)=>{
	let archivedProduct={
		isActive:false
	};
	return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then(product=>true).catch(saveErr=>{console.log(saveErr) 
	return false});
}

//activate a product
module.exports.activateProduct=(reqParams)=>{
	let activatedProduct={
		isActive:true
	};
	
	return Product.findByIdAndUpdate(reqParams.productId,activatedProduct).then(product=>true).catch(saveErr=>{console.log(saveErr) 
	return false});
}

module.exports.getOrders = async () => {
  try {
    const products = await Product.find({ userOrders: { $exists: true, $not: { $size: 0 } } });
    if (products.length > 0) {
      const orders = products.map(product => ({
        productId: product._id,
        productName: product.name,
        userOrders: product.userOrders
      }));
      return orders;
    } else {
      console.log("No products found with ordered products");
      return [];
    }
  } catch (error) {
    console.log(error);
    console.log("Error in retrieving products");
    return false;
  }
};



